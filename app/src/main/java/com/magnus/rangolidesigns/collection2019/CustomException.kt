package com.magnus.rangolidesigns.collection2019

class CustomException(message: String) : Exception(message)
